function validate(): void{

    // Extracting all co-ordinates
    var t11: HTMLInputElement= <HTMLInputElement>document.getElementById("t11");
    var t12: HTMLInputElement= <HTMLInputElement>document.getElementById("t12");

    var t21: HTMLInputElement= <HTMLInputElement>document.getElementById("t21");
    var t22: HTMLInputElement= <HTMLInputElement>document.getElementById("t22");

    var t31: HTMLInputElement= <HTMLInputElement>document.getElementById("t31");
    var t32: HTMLInputElement= <HTMLInputElement>document.getElementById("t32");

    var t41: HTMLInputElement= <HTMLInputElement>document.getElementById("t41");
    var t42: HTMLInputElement= <HTMLInputElement>document.getElementById("t42");

    var answer: HTMLDivElement= <HTMLDivElement>document.getElementById("div");

    var x1= +t11.value;
    var y1= +t12.value;
    var x2= +t21.value;
    var y2= +t22.value;
    var x3= +t31.value;
    var y3= +t32.value;
    var x= +t41.value;
    var y= +t42.value;

    var ans: String;

    // To check validity of co-ordinates
    if ((x1==x2 && x2==x3) || (y1==y2 && y2==y3))
    {
        alert("Co-ordinates Don't Form Triangle");
        return
    }

    //Error handling for non-numeric values entered
    if (isNaN(x1) || isNaN(y1) || isNaN(x2) || isNaN(y2) || isNaN(x3) || isNaN(y3) || isNaN(x) 
        || isNaN(y))
    {
        alert ("Please Enter Numeric Values Only !!");
        return;
    }

    // Calculating Area of Main Triangle
    var area: number= Math.abs( (x1*(y2-y3) + x2*(y3-y1) + x3*(y1-y2))/2 );

    // Calculating Area of 3 Triangles formed by joining given poing to 3 vertices
    var area1: number= Math.abs( ((x*(y1-y2)) + x1*(y2-y) + x2*(y-y1))/2 );
    var area2: number= Math.abs( ((x*(y2-y3)) + x2*(y3-y) + x3*(y-y2))/2 );
    var area3: number= Math.abs( ((x*(y1-y3)) + x1*(y3-y) + x3*(y-y1))/2 );

    // NOTE: Applied absolute function to make area positive


    /* Condition for checking position of point :
       Check if sum of 3 Triangles' Areas are equal to area of main triangle  */
    if ((area1+area2+area3 - area) < 0.0000001)
    {
        answer.innerHTML= "LIES INSIDE";
        answer.style.backgroundColor= "green";

    }
    else
    {
        answer.innerHTML= "LIES OUTSIDE"
        answer.style.backgroundColor= "darkred"
    }




}