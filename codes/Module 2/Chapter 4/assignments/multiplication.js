function mult() {
    var t1 = document.getElementById("t1");
    var tab = document.getElementById("tab");
    var x = +t1.value;
    // Error Handling for input of non-numeric value
    if (isNaN(x)) {
        alert("Enter a Numeric Value !!");
        return;
    }
    // To check if number is an integer
    if (x != Math.floor(x)) {
        alert("Please Enter Integer Values Only ");
        return;
    }
    // To check if number is positive
    if (x <= 0) {
        alert("Enter Positive Integers");
        return;
    }
    var count = 1;
    var body = document.body;
    // To clear previously obtained tables
    while (tab.rows.length > 0) {
        tab.deleteRow(0);
    }
    // Inserting First Row for the Header
    var row = tab.insertRow();
    var td = row.insertCell();
    td.setAttribute("colspan", "5"); // Merging the columns for the header
    td.style.height = "30px";
    var text = document.createElement("input");
    text.style.backgroundColor = "lightblue";
    text.style.textAlign = "center";
    text.style.width = "99%";
    text.style.height = "99%";
    text.style.font = "bold 18px";
    text.value = "MULTIPLICATION TABLE";
    td.appendChild(text);
    for (var i = 1; i <= x; i++) {
        var row = tab.insertRow(); // Inserting Row in the table
        // First Column
        var cell = row.insertCell(); // Inserting cells inside the row
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.style.backgroundColor = "cornflowerblue";
        text.style.width = "60px";
        text.value = x.toString();
        cell.appendChild(text); // Placing the text inside the cell
        // Second Column
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.style.backgroundColor = "cornflowerblue";
        text.style.width = "60px";
        text.value = "x";
        cell.appendChild(text);
        // Third Column
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.style.backgroundColor = "cornflowerblue";
        text.style.width = "60px";
        text.value = i.toString();
        cell.appendChild(text);
        // Fourth Column
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.style.backgroundColor = "cornflowerblue";
        text.style.width = "60px";
        text.value = "=";
        cell.appendChild(text);
        // Fifth Column 
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.style.backgroundColor = "cornflowerblue";
        text.style.width = "60px";
        text.value = (i * x).toString();
        cell.appendChild(text);
    }
}
//# sourceMappingURL=multiplication.js.map