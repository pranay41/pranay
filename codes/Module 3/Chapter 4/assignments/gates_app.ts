var canvas:HTMLCanvasElement= <HTMLCanvasElement>document.getElementById("mycanvas");
var context: CanvasRenderingContext2D= canvas.getContext('2d');

let a:Arc =new Arc(200,200,canvas,context);
a.drawarc();
let l1:Line = new Line(200,150,200,250,canvas,context);
l1.drawline();

let l2:Line = new Line(200,170,150,170,canvas,context);
l2.drawline();
let l3:Line = new Line(200,230,150,230,canvas,context);
l3.drawline();
let l4:Line = new Line(250,200,300,200,canvas,context);
l4.drawline();

let t:Triangle= new Triangle(200,400,canvas,context);
t.drawtriangle();
let ip:Line = new Line(200,450,150,450,canvas,context);
ip.drawline();
let op:Line = new Line(280,450,330,450,canvas,context);
op.drawline();

let txt1:Text1= new Text1("AND GATE",180,120,canvas,context);
txt1.writetext();
let txt2:Text1= new Text1("NOT GATE",180,370,canvas,context);
txt2.writetext();
