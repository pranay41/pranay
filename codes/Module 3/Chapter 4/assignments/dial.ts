class Dial{
    private canvas:HTMLCanvasElement;
    private context:CanvasRenderingContext2D;
    public x:number;
    public y:number;

    constructor(x:number,y:number,canvas:HTMLCanvasElement,context:CanvasRenderingContext2D)
    {
        this.x=x;
        this.y=y;
        this.canvas=canvas;
        this.context=context;
    }

    draw(){
        this.context.beginPath();
        this.context.arc(this.x,this.y,100,0,2*Math.PI,true);
        this.context.strokeStyle= "black";
        this.context.lineWidth= 4;
        this.context.font=" 10pt Calibri";
        this.context.fillStyle= "red";
        this.context.fillText("0",this.x,this.y-100);
        this.context.fillText("90",this.x+100,this.y);
        this.context.fillText("180",this.x,this.y+100);
        this.context.fillText("270",this.x-100,this.y);
        this.context.stroke();
    }

}

class Needle{
    private canvas:HTMLCanvasElement;
    private context:CanvasRenderingContext2D;
    public x:number;
    public y:number;
    public endx:number;
    public endy:number;
    private state:number = 0;

    constructor(x:number,y:number,canvas:HTMLCanvasElement,context:CanvasRenderingContext2D)
    {
        this.x=x;
        this.y=y;
        this.canvas=canvas;
        this.context=context;
    }

    setstate(state:number){
        this.state=state;
        if (this.state>=0 && this.state<=90)
        {
            this.endy=this.y-(100-this.state-10);
            this.endx=this.x+this.state
        }
        if (this.state>90 && this.state<=180)
        {
            this.endy=this.y+(this.state-90);
            this.endx=this.x+(this.state-90);
        }
        if (this.state>180 && this.state<=270)
        {
            this.endy=this.y+(100-this.state-180);
            this.endx=this.x-(this.state-180);
        }
        if (this.state>270 && this.state<=360)
        {
            this.endy=this.y-(this.state-270);
            this.endx=this.x-(this.state-270);
        }
    }

    draw(){
        this.context.beginPath();
        this.context.moveTo(this.x,this.y);
        this.context.lineTo(this.endx,this.endy);
        this.context.strokeStyle= "red"
        this.context.lineWidth= 4;
        this.context.stroke(); 
    }


}