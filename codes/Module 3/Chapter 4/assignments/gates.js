class Arc {
    constructor(x, y, canvas, context) {
        this.x = x;
        this.y = y;
        this.canvas = canvas;
        this.context = context;
    }
    drawarc() {
        console.log("hello");
        context.beginPath();
        context.arc(this.x, this.y, 50, Math.PI / 2, 3 * Math.PI / 2, true);
        context.fillStyle = "black";
        context.lineWidth = 3;
        context.stroke();
    }
}
class Line {
    constructor(x1, y1, x2, y2, canvas, context) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.canvas = canvas;
        this.context = context;
    }
    drawline() {
        console.log("hello");
        context.beginPath();
        context.moveTo(this.x1, this.y1);
        context.lineTo(this.x2, this.y2);
        context.fillStyle = "black";
        context.lineWidth = 3;
        context.stroke();
    }
}
class Triangle {
    constructor(x, y, canvas, context) {
        this.x = x;
        this.y = y;
        this.canvas = canvas;
        this.context = context;
    }
    drawtriangle() {
        console.log("hello");
        context.beginPath();
        context.moveTo(this.x, this.y);
        context.lineTo(this.x, this.y + 100);
        context.lineTo(this.x + 80, this.y + 50);
        context.lineTo(this.x, this.y);
        context.lineWidth = 3;
        context.fillStyle = "black";
        context.stroke();
    }
}
class Text1 {
    constructor(text, x, y, canvas, context) {
        this.text = text;
        this.x = x;
        this.y = y;
        this.canvas = canvas;
        this.context = context;
    }
    writetext() {
        context.font = '16pt Calibri';
        context.fillStyle = 'black';
        context.fillText(this.text, this.x, this.y);
    }
}
//# sourceMappingURL=gates.js.map