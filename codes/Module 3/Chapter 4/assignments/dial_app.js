var canvas = document.getElementById("mycanvas");
var context = canvas.getContext('2d');
var input = document.getElementById("t");
function draw() {
    let d = new Dial(150, 150, canvas, context);
    let n = new Needle(150, 150, canvas, context);
    context.clearRect(0, 0, canvas.width, canvas.height);
    d.draw();
    n.setstate(parseInt(input.value));
    n.draw();
}
//# sourceMappingURL=dial_app.js.map