var canvas:HTMLCanvasElement= <HTMLCanvasElement>document.getElementById("mycanvas");
var context: CanvasRenderingContext2D= canvas.getContext('2d');
var input:HTMLInputElement= <HTMLInputElement>document.getElementById("t");

function draw(){
    let d:Dial =new Dial(150,150,canvas,context);
    let n:Needle = new Needle(150,150,canvas,context);
    context.clearRect(0,0,canvas.width,canvas.height);
    d.draw();
    n.setstate(parseInt(input.value));
    n.draw();
}

