class Pendulum {
    constructor(x, y, canvas, context) {
        this.x = x;
        this.y = y;
        this.canvas = canvas;
        this.context = context;
    }
    draw() {
        this.context.beginPath();
        this.context.fillStyle = "black";
        context.fillStyle = "black";
        this.context.arc(this.x, this.y, 30, 0, 2 * Math.PI, true);
        context.lineWidth = 4;
        this.context.moveTo(this.x, this.y - 30);
        this.context.lineTo(this.x, this.y - 110);
        this.context.moveTo(this.x - 30, this.y - 110);
        this.context.lineTo(this.x + 30, this.y - 110);
        context.lineWidth = 6;
        this.context.strokeStyle = "black";
        context.fill();
        this.context.stroke();
    }
}
//# sourceMappingURL=pendulum.js.map