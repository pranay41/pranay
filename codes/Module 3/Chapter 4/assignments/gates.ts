class Arc{
    private canvas:HTMLCanvasElement;
    private context:CanvasRenderingContext2D;
    public x:number;
    public y:number;

    constructor(x:number,y:number,canvas:HTMLCanvasElement,context:CanvasRenderingContext2D)
    {
        this.x=x;
        this.y=y;
        this.canvas=canvas;
        this.context=context;
    }

    drawarc(){
        console.log("hello");
        context.beginPath();
        context.arc(this.x,this.y,50,Math.PI/2,3*Math.PI/2,true);
        context.fillStyle= "black"
        context.lineWidth= 3;
        context.stroke();
    }
}

class Line{
    private canvas:HTMLCanvasElement;
    private context:CanvasRenderingContext2D;
    public x1:number;
    public y1:number;
    public x2:number;
    public y2:number;

    constructor(x1:number,y1:number,x2:number,y2:number,canvas:HTMLCanvasElement,context:CanvasRenderingContext2D)
    {
        this.x1=x1;
        this.y1=y1;
        this.x2=x2;
        this.y2=y2;
        this.canvas=canvas;
        this.context=context;
    }

    drawline(){
        console.log("hello");
        context.beginPath();
        context.moveTo(this.x1,this.y1);
        context.lineTo(this.x2,this.y2);
        context.fillStyle= "black"
        context.lineWidth= 3;
        context.stroke();
    }
    
}

class Triangle{
    private canvas:HTMLCanvasElement;
    private context:CanvasRenderingContext2D;
    public x:number;
    public y:number;

    constructor(x:number,y:number,canvas:HTMLCanvasElement,context:CanvasRenderingContext2D)
    {
        this.x=x;
        this.y=y;
        this.canvas=canvas;
        this.context=context;
    }

    drawtriangle(){
        console.log("hello");
        context.beginPath();
        context.moveTo(this.x,this.y);
        context.lineTo(this.x,this.y+100);
        context.lineTo(this.x+80,this.y+50);
        context.lineTo(this.x,this.y);

        context.lineWidth= 3;
        context.fillStyle= "black";
        context.stroke();
    }

}

class Text1{
    private canvas:HTMLCanvasElement;
    private context:CanvasRenderingContext2D;
    public x:number;
    public y:number;
    public text:string;

    constructor(text:string,x:number,y:number,canvas:HTMLCanvasElement,context:CanvasRenderingContext2D)
    {
        this.text=text;
        this.x=x;
        this.y=y;
        this.canvas=canvas;
        this.context=context;
    }

    writetext(){
        context.font ='16pt Calibri';
        context.fillStyle= 'black';
        context.fillText(this.text, this.x, this.y);
    }
}